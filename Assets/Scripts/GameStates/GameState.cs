public class GameState {
    public virtual void Start() {}
    public virtual void Update() {}
    public virtual void FixedUpdate() {}
    public virtual void Exit() {}
}