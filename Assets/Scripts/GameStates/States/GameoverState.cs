using UnityEngine;
using Zenject;

public class GameoverState : GameState {
    private StateMachine stateMachine;
    private Tank playerTank;

    public GameoverState(StateMachine stateMachine, Tank playerTank) {
        this.stateMachine = stateMachine;
        this.playerTank = playerTank;
    }

    public override void Start() {
        GUIGameover menu = GameObject.FindObjectOfType<GUIGameover>();
        menu?.SetScore(this.playerTank.GetScore());
        menu?.Show();
    }

    public override void Exit() {
        GUIGameover menu = GameObject.FindObjectOfType<GUIGameover>();
        menu?.Hide();
    }

    public override void Update() { 
        if(Input.GetKeyDown(KeyCode.Space)) {
            this.stateMachine.ChangeState(States.GAMEPLAY);
        }
    }

    public class Factory : PlaceholderFactory<GameoverState> {}
}