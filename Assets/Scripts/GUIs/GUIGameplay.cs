using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GUIGameplay : GUIMenu
{
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;
    [SerializeField] private TMPro.TextMeshProUGUI livesText;
    [SerializeField] private Transform iconsGroup;
    [SerializeField] private GameObject liveIconPrefab;

    private Tank playerTank;

    private List<GameObject> icons;

    [Inject]
    public void Contruct(Tank playerTank) {
        this.playerTank = playerTank;
    }

    private void Awake() {
        Tank.onChangeScore += this.onChangeScore;    
        Tank.onHit += this.onHit;
    }

    public void Initialize() {
        this.setupScoreAndLivesText();
        this.createLiveIcons();
    }

    private void setupScoreAndLivesText() {
        this.scoreText.SetText("0000");
        this.livesText.SetText(this.playerTank.GetLives().ToString());
    }

    private void createLiveIcons() {
        this.icons = new List<GameObject>();
        for(int i = 0; i < this.playerTank.GetLives(); ++i) {
            GameObject iconObject = GameObject.Instantiate(this.liveIconPrefab, Vector3.zero, Quaternion.identity);
            iconObject.transform.parent = this.iconsGroup;
            iconObject.transform.localScale = Vector3.one;
            this.icons.Add(iconObject);
        }
    }

    private void onChangeScore(int score) {
        this.scoreText.SetText(string.Format("{0, 0:D4}", score));
    }

    private void onHit() {
        this.livesText.SetText(this.playerTank.GetLives().ToString());

        if(this.playerTank.GetLives() < 0)
            return;

        GameObject icon = this.icons.Last();
        this.icons.Remove(icon);
        GameObject.Destroy(icon);
    }
}
