using UnityEngine;
using Zenject;

public class MainMenuState : GameState {
    private StateMachine stateMachine;

    public MainMenuState(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }

    public override void Start() {
        GUIMainMenu menu = GameObject.FindObjectOfType<GUIMainMenu>();
        menu?.Show();
    }

    public override void Exit() {
        GUIMainMenu menu = GameObject.FindObjectOfType<GUIMainMenu>();
        menu?.Hide();
    }

    public override void Update() { 
        if(Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
        else if(Input.anyKeyDown) {
            this.stateMachine.ChangeState(States.GAMEPLAY);
        }
    }

    public class Factory : PlaceholderFactory<MainMenuState> {}
}