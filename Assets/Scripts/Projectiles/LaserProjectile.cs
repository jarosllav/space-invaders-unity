using System;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(Rigidbody2D))]
public class LaserProjectile : Projectile, IPoolable
{
    static public event Action<Invader> onHitInvader;

    private ObjectPool<LaserProjectile> originPool;
    private WorldHelper worldHelper;
    private Vector3 velocity;

    [Inject]
    public void Contruct(ObjectPool<LaserProjectile> originPool, WorldHelper worldHelper) {
        this.originPool = originPool;
        this.worldHelper = worldHelper;
    }

    private void FixedUpdate() {
        Vector3 position = velocity * Time.fixedDeltaTime + transform.position;
        transform.position = position;

        if(transform.position.y > this.worldHelper.Top) {
            originPool.Release(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Invader") {
            this.originPool.Release(this);

            Invader invader = other.gameObject.GetComponent<Invader>();
            LaserProjectile.onHitInvader?.Invoke(invader);
        }
        else if(other.gameObject.tag == "Bunker") {
            this.originPool.Release(this);
        }
    }

    public void SetVelocity(Vector3 velocity) {
        this.velocity = velocity;
    }

    void IPoolable.Prepare() {
        gameObject.SetActive(true);
    }
    
    void IPoolable.Dispose() {
        gameObject.SetActive(false);
    }

    public class Factory : PlaceholderFactory<LaserProjectile> { }
}