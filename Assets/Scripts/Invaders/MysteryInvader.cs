using UnityEngine;

public class MysteryInvader {
    public Invader invader;
    
    private WorldHelper worldHelper;
    private Settings settings;

    private float respawnTimer; 
    private bool spawned = false;

    public MysteryInvader(Settings settings, WorldHelper worldHelper) {
        this.settings = settings;
        this.worldHelper = worldHelper;
        this.respawnTimer = this.settings.respawnTime;
    }

    public void Update() {
        if(this.respawnTimer <= 0) {
            if(!this.spawned) {
                this.Spawn();
            }
            else {
                this.updateMovement();
            }
        }
        else {
            this.respawnTimer -= Time.deltaTime;
        }
    }

    public void Spawn() {
        float x = this.worldHelper.Right + this.invader.GetComponent<SpriteRenderer>().bounds.size.x;
        float y = this.worldHelper.Top - this.invader.GetComponent<SpriteRenderer>().bounds.size.y;

        Vector3 position = new Vector3(x, y, 0f);
        this.invader.transform.position = position;
        this.invader.gameObject.SetActive(true);
        this.spawned = true;
    }

    public void Unspawn() {
        this.invader.gameObject.SetActive(false);
        this.respawnTimer = this.settings.respawnTime;
        this.spawned = false;
    }

    private void updateMovement() {
        this.invader.transform.position += Vector3.left * this.settings.speed * Time.deltaTime;

        if(this.invader.transform.position.x < this.worldHelper.Left) {
            this.Unspawn();
        }
    }

    [System.Serializable]
    public class Settings {
        public float speed;
        public float respawnTime;
    }
}