using System;
using UnityEngine;
using Zenject;

public class Tank : MonoBehaviour {
    static public event Action onHit;
    static public event Action onHitInvader;
    static public event Action onDestroy;
    static public event Action<int> onChangeScore;

    public bool isHandleInput = false;

    [SerializeField] private Transform projectilePosition;

    private Settings settings;
    private WorldHelper worldHelper;
    private ObjectPool<LaserProjectile> laserPool;

    private float attackTimer = 0f;
    private float xVelocity = 0f;
    private int lives = 0;
    private int score = 0;

    [Inject]
    public void Construct(Settings settings, WorldHelper worldHelper, LaserProjectile.Factory laserFactory, ObjectPool<LaserProjectile> laserPool) {
        this.settings = settings;
        this.worldHelper = worldHelper;
        this.laserPool = laserPool;
        this.laserPool.factory = laserFactory.Create;

        this.lives = this.settings.startLives;

        InvaderManager.onDestroyInvader += this.onDestroyInvader;
        InvaderManager.onDestroyAllInvaders += this.onDestroyAllInvaders;
    }

    private void Update() {
        if(!this.canShot())
            this.attackTimer -= Time.deltaTime;

        if(this.isHandleInput)
            this.handleInput();
    }

    private void FixedUpdate() {
        if(this.xVelocity != 0f)         
            this.applyVelocity();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Invader") {
            Tank.onHitInvader?.Invoke();
        }
    }

    // Public methods
    public void Reset() {
        this.lives = this.settings.startLives;
        this.score = 0;
    }

    public void Hit() {
        this.lives -= 1;

        if(this.lives <= 0) {
            Tank.onDestroy?.Invoke();
        }
        Tank.onHit?.Invoke();
    }

    public int GetLives() { return this.lives; }

    public int GetScore() { return this.score; }

    // Private methods
    private void applyVelocity() {
        Vector3 movement = new Vector3(this.xVelocity * Time.fixedDeltaTime, 0f, 0f);
        Vector3 position = transform.position + movement;

        float halfWidth = GetComponent<SpriteRenderer>().bounds.size.x / 2f;
        if(position.x < this.worldHelper.Left + halfWidth)
            position.x = this.worldHelper.Left + halfWidth;
        else if(position.x > this.worldHelper.Right - halfWidth)
            position.x = this.worldHelper.Right - halfWidth;

        transform.position = position;
    }

    private void handleInput() {
        float horizontal = Input.GetAxis("Horizontal");
        this.xVelocity = horizontal * this.settings.moveSpeed;

        if(this.canShot() && Input.GetButtonDown("Fire1")) {
            this.shotProjectile();
            this.attackTimer = this.settings.attackSpeed;
        }
    }

    private bool canShot() {
        if(this.attackTimer <= 0f)
            return true;
        return false;
    }

    private void shotProjectile() {
        LaserProjectile laserProjectile = laserPool.Get();
        laserProjectile.SetVelocity(Vector3.up * this.settings.projectileSpeed);
        laserProjectile.transform.position = this.projectilePosition.position;
    }

    private void onDestroyInvader(Invader invader) {
        this.score += invader.rewardPoints;
        Tank.onChangeScore?.Invoke(this.score);
    }

    private void onDestroyAllInvaders() {
        LaserProjectile[] projectiles = GameObject.FindObjectsOfType<LaserProjectile>();
        foreach(LaserProjectile projectile in projectiles) {
            this.laserPool.Release(projectile);
        }
    }

    [Serializable]
    public class Settings {
        public float attackSpeed;
        public float moveSpeed;
        public float projectileSpeed;
        public int startLives;
    }    
}