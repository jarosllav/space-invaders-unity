using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIGameover : GUIMenu
{
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;

    public void SetScore(int score) {
        this.scoreText?.SetText(string.Format("{0, 0:D4}", score));
    }
}
