using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public enum InvaderType {
    Small, Medium, Large, Mystery
}

public class Invader : MonoBehaviour {
    public InvaderType type;
    public int rewardPoints = 0;

    private Settings settings;

    [Inject]
    public void Contruct(Settings settings) {
        this.settings = settings;
    }

    [Serializable]
    public class Settings {
        // ...
    }

    public class Factory : PlaceholderFactory<InvaderType, Invader> {}

    public class TypeFactory : IFactory<InvaderType, Invader> {
        private DiContainer container;
        private Settings settings;
        private Dictionary<InvaderType, GameObject> gameObjectToType;

        public TypeFactory(DiContainer container, Settings settings) {
            this.container = container;
            this.settings = settings;
            this.gameObjectToType = this.createGameObjectDictionary();
        }

        public Invader Create(InvaderType type) {
            switch(type) {
                case InvaderType.Medium:
                    return this.container.InstantiatePrefabForComponent<Invader>(
                        this.gameObjectToType[InvaderType.Medium]);
                case InvaderType.Large:
                    return this.container.InstantiatePrefabForComponent<Invader>(
                        this.gameObjectToType[InvaderType.Large]);
                case InvaderType.Mystery:
                    return this.container.InstantiatePrefabForComponent<Invader>(
                        this.gameObjectToType[InvaderType.Mystery]);
            }

            return this.container.InstantiatePrefabForComponent<Invader>(
                        this.gameObjectToType[InvaderType.Small]);
        }

        private Dictionary<InvaderType, GameObject> createGameObjectDictionary() {
            Dictionary<InvaderType, GameObject> gameObjectDictionary = new Dictionary<InvaderType, GameObject>();
            foreach(GameObject gameObject in this.settings.invaderPrefabs) {
                gameObjectDictionary.Add(gameObject.GetComponent<Invader>().type, gameObject);
            }
            return gameObjectDictionary;
        }

        [Serializable]
        public class Settings {
            public List<GameObject> invaderPrefabs;
        }
    }
}