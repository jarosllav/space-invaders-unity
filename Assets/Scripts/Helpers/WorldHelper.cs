using UnityEngine;
using Zenject;

public class WorldHelper {
    private Camera camera;

    public WorldHelper([Inject(Id = "MainCamera")] Camera camera) {
        this.camera = camera;
    }

    public float Bottom {
        get {return -HalfHeight; }
    }

    public float Top {
        get {return HalfHeight; }
    }

    public float Left {
        get {return -HalfWidth; }
    }

    public float Right {
        get {return HalfWidth; }
    }

    public float HalfHeight {
        get { return this.camera.orthographicSize; }
    }

    public float HalfWidth {
        get { return this.camera.aspect * this.camera.orthographicSize; }
    }

    public float Height {
        get { return this.HalfHeight * 2f; }
    }

    public float Width {
        get { return this.HalfWidth * 2f; }
    }
}