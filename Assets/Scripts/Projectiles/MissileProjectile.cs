using System;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(Rigidbody2D))]
public class MissileProjectile : Projectile, IPoolable
{
    private ObjectPool<MissileProjectile> originPool;
    private WorldHelper worldHelper;
    private Vector3 velocity;

    [Inject]
    public void Contruct(ObjectPool<MissileProjectile> originPool, WorldHelper worldHelper) {
        this.originPool = originPool;
        this.worldHelper = worldHelper;
    }

    private void FixedUpdate() {
        Vector3 position = velocity * Time.fixedDeltaTime + transform.position;
        transform.position = position;

        if(transform.position.y < this.worldHelper.Bottom) {
            originPool.Release(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player") {
            Tank tank = other.GetComponent<Tank>();
            tank.Hit();

            this.originPool.Release(this);
        }
        else if(other.gameObject.tag == "Bunker") {
            this.originPool.Release(this);
        }
    }

    public void SetVelocity(Vector3 velocity) {
        this.velocity = velocity;
    }

    void IPoolable.Prepare() {
        gameObject.SetActive(true);
    }
    
    void IPoolable.Dispose() {
        gameObject.SetActive(false);
    }

    public class Factory : PlaceholderFactory<MissileProjectile> { }
}