using UnityEngine;
using Zenject;

public class GameplayState : GameState {
    private StateMachine stateMachine;
    private Tank playerTank;
    private InvaderManager invaderManager;
    private BunkerManager bunkerManager;

    public GameplayState(StateMachine stateMachine, Tank playerTank, InvaderManager invaderManager, BunkerManager bunkerManager) {
        this.stateMachine = stateMachine;
        this.playerTank = playerTank;
        this.invaderManager = invaderManager;
        this.bunkerManager = bunkerManager;
    }

    public override void Start() {
        this.setupGame();

        GUIGameplay menu = GameObject.FindObjectOfType<GUIGameplay>();
        menu?.Initialize();
        menu?.Show();

        Tank.onHit += this.onPlayerHit;
        Tank.onHitInvader += this.onPlayerHitInvader;
    }

    public override void Exit() {
        GUIGameplay menu = GameObject.FindObjectOfType<GUIGameplay>();
        menu?.Hide();

        this.playerTank.isHandleInput = false;
        Tank.onHit -= this.onPlayerHit;
        Tank.onHitInvader -= this.onPlayerHitInvader;
    }

    public override void Update() { 
        this.invaderManager.Update();
    }

    private void setupGame() {
        this.setupPlayerStartPosition();
        this.playerTank.Reset();
        this.playerTank.isHandleInput = true;

        this.invaderManager.CreateInvaders();
        this.bunkerManager.CreateBunkers();
    }

    private void setupPlayerStartPosition() {
        Vector3 position = new Vector3(0f, -Camera.main.orthographicSize + 1f, 0f);
        this.playerTank.transform.position = position;
    }

    private void onPlayerHit() {
        if(this.playerTank.GetLives() <= 0) {
            this.stateMachine.ChangeState(States.GAMEOVER);
        }
    }

    private void onPlayerHitInvader() {
        this.stateMachine.ChangeState(States.GAMEOVER);
    }

    public class Factory : PlaceholderFactory<GameplayState> {}
}