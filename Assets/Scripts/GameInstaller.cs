using System;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [Inject]
    private Settings settings;

    public override void InstallBindings() {
        this.installHelpers();
        this.installInvaders();
        this.installTank();  
        this.installProjectiles();  
        this.installGameStates();
    }

    private void installInvaders() {
        Container.BindInterfacesAndSelfTo<InvaderManager>().AsSingle();
        Container.BindFactory<InvaderType, Invader, Invader.Factory>()
            .FromFactory<Invader.TypeFactory>();
        Container.Bind<MysteryInvader>().AsSingle();
    }

    private void installTank() {
        Container.Bind<Tank>()
            .FromComponentInNewPrefab(this.settings.tankPrefab)
            .WithGameObjectName("Tank")
            .AsSingle();

        Container.Bind<BunkerManager>().AsSingle();
    }

    private void installProjectiles() {
        Container.BindFactory<LaserProjectile, LaserProjectile.Factory>()
            .FromComponentInNewPrefab(this.settings.laserPrefab);
        Container.BindInstance(new ObjectPool<LaserProjectile>()).AsSingle();

        Container.BindFactory<MissileProjectile, MissileProjectile.Factory>()
            .FromComponentInNewPrefab(this.settings.missilePrefab);
        Container.BindInstance(new ObjectPool<MissileProjectile>()).AsSingle();
    }

    private void installHelpers() {
        Container.Bind<Camera>().WithId("MainCamera").FromInstance(Camera.main);
        Container.Bind<WorldHelper>().AsSingle();
    }

    private void installGameStates() {
        Container.Bind<StateMachine>().AsSingle();
        Container.Bind<GameStateFactory>().AsSingle();

        Container.BindInterfacesAndSelfTo<MainMenuState>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameplayState>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameoverState>().AsSingle();
        Container.BindFactory<MainMenuState, MainMenuState.Factory>().WhenInjectedInto<GameStateFactory>();
        Container.BindFactory<GameplayState, GameplayState.Factory>().WhenInjectedInto<GameStateFactory>();
        Container.BindFactory<GameoverState, GameoverState.Factory>().WhenInjectedInto<GameStateFactory>();
    }

    [Serializable]
    public class Settings {
        public GameObject tankPrefab;
        public GameObject laserPrefab;
        public GameObject missilePrefab;
    }
}