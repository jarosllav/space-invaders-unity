using System;
using UnityEngine;
using Zenject;

//[CreateAssetMenu(fileName = "GameScriptableInstaller", menuName = "Installers/GameScriptableInstaller")]
public class GameScriptableInstaller : ScriptableObjectInstaller<GameScriptableInstaller>
{
    public Invader.Settings invaderSettings;
    public MysteryInvader.Settings mysteryInvaderSettings;
    public InvaderManager.Settings invaderManagerSettings;
    public Invader.TypeFactory.Settings invaderFactorySettings;
    public Tank.Settings tankSettings;
    public BunkerManager.Settings bunkersSettings;
    public GameInstaller.Settings gameInstallerSettings;

    public override void InstallBindings() {
        Container.BindInstance(this.invaderSettings);
        Container.BindInstance(this.mysteryInvaderSettings);
        Container.BindInstance(this.invaderManagerSettings);
        Container.BindInstance(this.invaderFactorySettings);
        Container.BindInstance(this.tankSettings);
        Container.BindInstance(this.bunkersSettings);
        Container.BindInstance(this.gameInstallerSettings);
    }
}