using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    private StateMachine stateMachine;
    private InvaderManager invaderManager;
    private Tank player;

    [Inject]
    public void Construct(StateMachine stateMachine, InvaderManager invaderManager, Tank player) {
        this.stateMachine = stateMachine;
        this.invaderManager = invaderManager;
        this.player = player;
    }

    private void Start() {
        this.stateMachine.ChangeState(States.MAIN);
    }

    private void Update() {
        this.stateMachine.Update();
    }

    private void FixedUpdate() {
        this.stateMachine.FixedUpdate();
    }
}
