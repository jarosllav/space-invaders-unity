using UnityEngine;
using Zenject;

public class BunkerManager {
    private Settings settings;
    private WorldHelper worldHelper;

    public BunkerManager(Settings settings, WorldHelper worldHelper) {
        this.settings = settings;
        this.worldHelper = worldHelper;
    }

    public void CreateBunkers() {
        float yPosition = this.worldHelper.Bottom + this.settings.bunkerPrefab.GetComponent<SpriteRenderer>().bounds.size.y + this.settings.yOffset;
        float offset = this.worldHelper.Width / (this.settings.bunkersCount + 1);

        for(int i = 0; i < this.settings.bunkersCount; ++i) {
            GameObject bunker = GameObject.Instantiate(this.settings.bunkerPrefab);
            bunker.transform.position = new Vector3(this.worldHelper.Left + (i + 1) * offset, yPosition, 0f);
        }
    }

    [System.Serializable]
    public class Settings {
        public GameObject bunkerPrefab;
        public int bunkersCount = 4;
        public float yOffset = 1;
    }
}