using System;
using System.Collections.Generic;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;
using Zenject;

public class InvaderManager
{
    static public event Action onDestroyAllInvaders;
    static public event Action<Invader> onDestroyInvader;

    private Settings settings;
    private Invader.Factory invaderFactory;
    private ObjectPool<MissileProjectile> missilePool;
    private WorldHelper worldHelper;

    private Transform invadersGroup;
    private List<Invader> invaders;
    private MysteryInvader mysteryInvader;
    private float attackTimer;
    private Vector3 direction = Vector2.right;

    public InvaderManager(Settings settings, WorldHelper worldHelper, MysteryInvader mysteryInvader, Invader.Factory invaderFactory, MissileProjectile.Factory missileFactory, ObjectPool<MissileProjectile> missilePool) {
        this.settings = settings;
        this.worldHelper = worldHelper;
        this.mysteryInvader = mysteryInvader;
        this.missilePool = missilePool;
        this.invaderFactory = invaderFactory;

        this.missilePool.factory = missileFactory.Create;
        this.invadersGroup = this.createInvadersGroup();
        this.mysteryInvader.invader = this.createMysteryInvader();
    
        LaserProjectile.onHitInvader += this.onHitInvader;
    }

    public void CreateInvaders() {
        this.clearInvadersList();
        this.spawnInvadersFromSettings();
        this.setupStartPosition();
    }

    public void Update() {
        if(!this.canShot()) {
            this.attackTimer -= Time.deltaTime;
        }
        else {
            this.attackTimer = this.settings.attackSpeed;
            this.shotProjectileFromRandomInvader();
        }

        this.updateMovement();
        this.mysteryInvader?.Update();
    }

    private void updateMovement() {
        Vector3 movement = this.direction * this.settings.invadersSpeed * Time.deltaTime;

        foreach(Invader invader in this.invaders) {
            invader.transform.position += movement;

            Vector3 position = invader.transform.position;
            if(direction == Vector3.right && position.x > this.worldHelper. Right) {
                direction = Vector3.left;
                this.moveDown();
            }
            if(direction == Vector3.left && position.x < this.worldHelper.Left) {
                direction = Vector3.right;
                this.moveDown();
            }
        }
    }

    private void moveDown() {
        foreach(Invader invader in this.invaders) {
            invader.transform.Translate(Vector3.down * 0.25f);
        }
    }

    private void spawnInvadersFromSettings() {
        for(int y = 0; y < this.settings.height; ++y) {
            InvaderType invaderType = this.getInvaderTypeLayer(this.settings.height - 1 - y);
            for(int x = 0; x < this.settings.width; ++x) {
                Vector3 position = new Vector3(x, y, 0);

                Invader invader = this.invaderFactory.Create(invaderType);
                invader.transform.parent = this.invadersGroup;
                invader.transform.localPosition = position;

                this.invaders.Add(invader);
            }
        }
    }

    private void setupStartPosition() {
        Vector3 position = new Vector3(-Camera.main.orthographicSize, 0f, 0f);
        this.invadersGroup.position = position;
    }

    private Transform createInvadersGroup() {
        GameObject groupObject = new GameObject("Invaders");
        return groupObject.transform;
    }

    private Invader createMysteryInvader() {
        Invader mystery = this.invaderFactory.Create(InvaderType.Mystery);
        mystery.gameObject.SetActive(false);
        return mystery;
    }

    private void clearInvadersList() {
        if(this.invaders != null) {
            for(int i = this.invaders.Count - 1; i >= 0; --i) {
                GameObject.Destroy(this.invaders[i].gameObject);
            }
            this.invaders.Clear();
        }
        else {
            this.invaders = new List<Invader>();
        }
    }

    private InvaderType getInvaderTypeLayer(int y) {
        Assert.IsFalse((y < 0) || (y >= this.settings.layers.Length));
        return this.settings.layers[y];
    }

    private void onHitInvader(Invader invader) {
        if(invader == this.mysteryInvader.invader) {
            this.onHitMysteryInvader();
            return;
        }

        if(!this.invaders.Contains(invader))
            return;
        
        InvaderManager.onDestroyInvader?.Invoke(invader);
        GameObject.Destroy(invader.gameObject);
        this.invaders.Remove(invader);

        if(this.invaders.Count <= 0) {
            this.spawnInvadersFromSettings();
            this.clearAllMissiles();
            this.setupStartPosition();
            InvaderManager.onDestroyAllInvaders?.Invoke();
        }
    }

    private void onHitMysteryInvader() {
        InvaderManager.onDestroyInvader?.Invoke(this.mysteryInvader.invader);
        this.mysteryInvader.Unspawn();
    }

    private bool canShot() {
        if(this.attackTimer <= 0f)
            return true;
        return false;
    }

    private void shotProjectileFromRandomInvader() {
        Invader invader = this.invaders[UnityEngine.Random.Range(0, this.invaders.Count)];
        this.shotProjectileFromInvader(invader);
    }

    private void shotProjectileFromInvader(Invader invader) {
        Vector3 misslePosition = invader.transform.position;
        MissileProjectile missileProjectile = missilePool.Get();
        missileProjectile.SetVelocity(Vector3.down * this.settings.missileSpeed);
        missileProjectile.transform.position = misslePosition;
    }

    private void clearAllMissiles() {
        MissileProjectile[] projectiles = GameObject.FindObjectsOfType<MissileProjectile>();
        foreach(MissileProjectile projectile in projectiles) {
            this.missilePool.Release(projectile);
        }
    }

    [Serializable]
    public class Settings {
        public int width;
        public int height;
        public InvaderType[] layers;
        public float attackSpeed;
        public float missileSpeed;
        public float invadersSpeed;
    }
}