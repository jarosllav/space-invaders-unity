using UnityEngine; 

public class GameStateFactory {
    private MainMenuState.Factory mainMenuFactory;
    private GameplayState.Factory gameplayFactory;
    private GameoverState.Factory gameoverFactory;

    public GameStateFactory(MainMenuState.Factory mainMenuFactory, GameplayState.Factory gameplayFactory, GameoverState.Factory gameoverFactory) {
        this.mainMenuFactory = mainMenuFactory;
        this.gameplayFactory = gameplayFactory;
        this.gameoverFactory = gameoverFactory;
    }
    
    public GameState Create(States state) {
        switch(state) {
            case States.MAIN:
                return this.mainMenuFactory.Create();
            case States.GAMEPLAY:
                return this.gameplayFactory.Create();
            case States.GAMEOVER:
                return this.gameoverFactory.Create();
        }
        return null;
    }
}